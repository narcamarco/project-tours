import React, { useEffect, useRef, useState } from 'react';
import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
import './Map.css';

const Map = (props) => {
  mapboxgl.accessToken = process.env.REACT_APP_ACCESS_TOKEN;

  const { center, zoom } = props;
  console.log(center);

  const mapContainer = useRef(null);

  const lng = useState(center[0])[0];
  const lat = useState(center[1])[0];

  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/streets-v12',
      center: [lng, lat],
      zoom: zoom,
    });

    new mapboxgl.Marker({ position: [lng, lat], map: map });

    // const map = new mapboxgl.Map({
    //   container: mapRef.current,
    //   style: 'mapbox://styles/mapbox/streets-v11',
    //   center: center,
    //   zoom: zoom,
    // });
  }, [lng, lat, zoom]);

  return (
    <div
      ref={mapContainer}
      className={`map ${props.className}`}
      style={props.style}
    ></div>
  );
};

export default Map;
