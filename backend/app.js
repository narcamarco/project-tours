const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const helmet = require('helmet');

const placesRoutes = require('./routes/places-routes');
const usersRoutes = require('./routes/users-routes');

const app = express();

app.use(bodyParser.json());
app.use(
  cors({
    credentials: true,
    origin: 'https://project-tours.vercel.app',
    optionsSuccessStatus: 200,
  })
);
app.use(helmet());
app.use(helmet.crossOriginResourcePolicy({ policy: 'cross-origin' }));

app.use(
  '/uploads/images',
  express.static(path.join(__dirname, 'uploads', 'images'))
);
app.use(express.static(path.join(__dirname, '../frontend', 'build')));

app.use('/api/places', placesRoutes);
app.use('/api/users', usersRoutes);

app.use((req, res, next) => {
  res.sendFile(path.path(__dirname, '../frontend', 'build', 'index.html'));
});

app.use((error, req, res, next) => {
  console.log(`${res.headersSent} Error middleware`);
  console.log(error.message);

  if (req.file) {
    fs.unlink(req.file.path, (err) => {
      console.log(err);
    });
  }

  if (res.headerSent) {
    return next(error);
  }

  res
    .status(error.code || 500)
    .json({ message: error.message || 'An Unknown error occured' });
});

mongoose
  .connect(process.env.DB_URL)
  .then(() => {
    const PORT = process.env.PORT || 5000;
    app.listen(PORT, () => {
      console.log(`Server is connecting to the port ${PORT}...`);
    });
  })
  .catch((err) => {
    console.log(err);
  });
