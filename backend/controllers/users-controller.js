const { validationResult } = require('express-validator');
const HttpError = require('../models/http-error');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

const getUsers = async (req, res, next) => {
  let users;
  try {
    users = await User.find({}).select('-password');
  } catch (err) {
    const error = new HttpError(
      `Fetching users failed, please try again later`,
      500
    );

    return next(error);
  }

  res
    .status(200)
    .json({ users: users.map((user) => user.toObject({ getters: true })) });
};

const signUp = async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    console.log(errors);
    return next(
      new HttpError('Invalid inputs passed, please check your data', 422)
    );
  }

  const { name, email, password } = req.body;

  let existingUser;

  try {
    existingUser = await User.findOne({ email: email });
  } catch (err) {
    const error = new HttpError(
      `Signing up failed, please try again later`,
      500
    );

    return next(error);
  }

  if (existingUser) {
    const error = new HttpError(
      `User exists already, please login instead`,
      422
    );

    return next(error);
  }

  let hashedPassword;

  try {
    hashedPassword = await bcrypt.hash(password, 12);
  } catch (err) {
    const error = new HttpError(`Could not create user. please try again`, 500);

    return next(error);
  }

  const createdUser = new User({
    name,
    email,
    image: req.file.filename,
    password: hashedPassword,
    places: [],
  });

  try {
    await createdUser.save();
  } catch (err) {
    const error = new HttpError(`Signing up failed. please try again`, 500);

    return next(error);
  }

  let token;
  try {
    token = jwt.sign(
      { userId: createdUser.id, email: createdUser.email },
      process.env.JWT_SECRET_KEY,
      {
        expiresIn: process.env.JWT_EXPIRES_IN,
      }
    );
  } catch (err) {
    const error = new HttpError(`Signing up failed. please try again`, 500);

    return next(error);
  }

  res
    .status(201)
    .json({ userId: createdUser.id, email: createdUser.email, token: token });
};

const login = async (req, res, next) => {
  const { email, password } = req.body;

  let existingUser;

  try {
    existingUser = await User.findOne({ email: email });
  } catch (err) {
    const error = new HttpError(
      `Logging In failed, please try again later`,
      500
    );

    return next(error);
  }

  if (!existingUser) {
    const error = new HttpError(
      `Invalid Credentials, could not logged you in`,
      401
    );

    return next(error);
  }

  let isValidPassword = false;
  try {
    isValidPassword = await bcrypt.compare(password, existingUser.password);
  } catch (err) {
    const error = new HttpError(
      `Could not log you in, please check your credentials and try again`,
      401
    );

    return next(error);
  }

  if (!isValidPassword) {
    const error = new HttpError(
      `Could not log you in, please check your credentials and try again`,
      401
    );

    return next(error);
  }

  let token;
  try {
    token = jwt.sign(
      { userId: existingUser.id, email: existingUser.email },
      process.env.JWT_SECRET_KEY,
      {
        expiresIn: process.env.JWT_EXPIRES_IN,
      }
    );
  } catch (err) {
    const error = new HttpError(`Logging In failed. please try again`, 500);

    return next(error);
  }

  res.status(200).json({
    userId: existingUser.id,
    email: existingUser.email,
    token: token,
  });
};

exports.getUsers = getUsers;
exports.signUp = signUp;
exports.login = login;
