const axios = require('axios');
const HttpError = require('../models/http-error');

const API_LINK = 'https://api.mapbox.com/geocoding/v5/mapbox.places';

const API_KEY = process.env.GOOGLE_API_KEY;

const PARAM = '&autocomplete=true&fuzzyMatch=false';

module.exports = async (address) => {
  const searchText = encodeURIComponent(address);
  const urlForGeoCode = `${API_LINK}/${searchText}.json?access_token=${API_KEY}${PARAM}`;

  const response = await axios.get(urlForGeoCode);

  if (!response.data.features.length) {
    const error = new HttpError(
      `Could not find location for the specified address`,
      422
    );

    throw error;
  }

  const coordinates = response.data.features[0].center;

  return {
    lat: coordinates[1],
    lng: coordinates[0],
  };
};
